from random import choice
from datetime import datetime

RANDOM_QUOTES = [
    {"author": "Jules", "quote": "English mother fucker, do you speak it?"},
    {"author": "Mia Wallace", "quote": "I said Goddamn!"},
    {"author": "Paul", "quote": "Hey, man, my name's Paul, and that shit's between y'all."},
    {"author": "Vincent Vega", "quote": "Aw, man, I shot Marvin in the face."},
    {"author": "Fabienne", "quote": "What happened to my Honda?"},
    {"author": "Vincent Vega", "quote": "God damn that's a pretty fucking good milkshake."},
    {"author": "Butch Coolidge", "quote": "Zed's dead, baby. Zed's dead."},
]


def get_random_quote() -> str:
    """ Retrieves a random quote
    """
    chosen_quote = choice(RANDOM_QUOTES)
    return f'"{chosen_quote["quote"]}" -- {chosen_quote["author"]}'


def print_quote(quote: str) -> None:
    """Prints a supplied quote with a prefix
    """
    message_prefix = f'Quote of the time! {datetime.now()}:'
    full_message = f'{message_prefix} {quote}'
    print(full_message)


def main() -> None:
    print_quote(get_random_quote())


if __name__ == "__main__":
    main()
