FROM python:3-alpine

WORKDIR /usr/src/app

RUN apk add --no-cache --virtual .build-deps gcc musl-dev

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

RUN apk del .build-deps gcc musl-dev

COPY . .

CMD [ "python3", "./print_quote.py" ]